
import {Ingredient} from "../models/ingredient";

export class ShoppingListService{
    private ingredients: Ingredient[]= [];
//add a single item
    addItem(name: string, amount: number){
        this.ingredients.push(new Ingredient(name, amount));
        console.log(this.ingredients);
    }

    //add multiple items
    addItems(items: Ingredient[]){
        this.ingredients.push(...items);
    }

    //retrieve the ingredient array, return the copy of the array

    getItems(){
        return this.ingredients.slice();
    }

    //remove the item

    removeItem(index: number){
        this.ingredients.splice(index, 1);
    }
}